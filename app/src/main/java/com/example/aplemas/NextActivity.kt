package com.example.aplemas

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.app_bar_main.*

class NextActivity : AppCompatActivity() {

    lateinit var fab_btn: FloatingActionButton;
    var TAG = "NextActivity";

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.next_activity)

        fab_btn = findViewById(R.id.fab)

        fab_btn.setOnClickListener {
            Toast.makeText(this, "toast message with gravity",Toast.LENGTH_SHORT).show()
        }

        main()
    }

    fun main(){
        println("surya hanggara")
        Log.e(TAG, "main: asyik")
    }
}