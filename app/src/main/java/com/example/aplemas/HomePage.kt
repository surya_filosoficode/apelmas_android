package com.example.aplemas

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

class HomePage : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.home_page)
    }
}